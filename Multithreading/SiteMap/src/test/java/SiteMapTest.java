import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class SiteMapTest {

    public static final String pathToTestCasesResult = "./src/test/results";

    @BeforeClass
    public static void initial() {
        File resDir = new File(pathToTestCasesResult);

        if (!resDir.exists()) {
            resDir.mkdir();
        }
    }

    @Test
    public void testWithSkillbox() {
        String link = "https://skillbox.ru/";

        String fileForSkillboxRes = "/skillboxMap.txt";
        File skillboxRes = new File(pathToTestCasesResult + fileForSkillboxRes);
        try {
            if (!skillboxRes.exists()) {
                skillboxRes.createNewFile();
            }
            PrintWriter writer = new PrintWriter(skillboxRes);

            PageLinks skillboxLinks = new PageLinks(link);
            String result = skillboxLinks.compute();

            writer.println(result);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
