import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        String link = "https://skillbox.ru/";
//        String link = "https://lenta.ru/";
        String linkTest = "https://lenta.ru/news/2022/03/21/72_suffered/";
        String strForLink = "//";
        Pattern p = Pattern.compile(strForLink);
        Matcher m = p.matcher(link);
        String[] splittedLink = p.split(link);
        System.out.println(splittedLink[1]);

        File file = new File("./workResult.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        System.out.println(file.getAbsolutePath());
        PrintWriter writer = new PrintWriter(file);

        PageLinks pageLinks = new PageLinks(link);
        String result = pageLinks.compute();
        writer.print(result);

        writer.flush();
        writer.close();

    }
}
