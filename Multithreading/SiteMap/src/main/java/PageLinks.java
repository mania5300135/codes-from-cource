import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.IOException;
import java.util.*;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PageLinks extends RecursiveTask<String> {

    private final String link;
    private final String mainLink;
    public static HashSet<String> metLinks;
    private final int depth;
    private String firstPartMainLink;
    private String secondPartMainLink;

    public PageLinks(String link, String mainLink, int depth) {
        this.link = link;
        this.depth = depth;
        this.mainLink = mainLink;
        parseMainLink();
    }

    public PageLinks(String mainLink) {
        this.link = mainLink;
        this.mainLink = mainLink;
        metLinks = new HashSet<>();
        metLinks.add(link);
        depth = 0;
        parseMainLink();
    }

    private void parseMainLink() {
        String regexForMainLink = "//";
        Pattern p = Pattern.compile(regexForMainLink);
        String[] arrayParts = p.split(mainLink);

        firstPartMainLink = arrayParts[0];
        secondPartMainLink = arrayParts[1];
    }

    @Override
    protected String compute() {
        String finalStr = "";
        try {
            List<PageLinks> newTasks = createNewTasks();
            if (!newTasks.isEmpty()) {
                StringBuilder childResult = new StringBuilder();
                childResult.append(makeString().concat("\n"));
                Collection<PageLinks> result = ForkJoinTask.invokeAll(newTasks);
                for (PageLinks link : result) {
                    childResult.append(link.join().concat("\n"));
                }
                finalStr = childResult.toString();
            }
            else {
                finalStr = makeString();
            }
        }
        catch (HttpStatusException e) {
            finalStr = makeString();
            return finalStr + " Ошибка 404";
        }
        catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return finalStr;
    }

    private List<PageLinks> createNewTasks() throws IOException, InterruptedException {
        List<PageLinks> newTasks = new ArrayList<>();

        Document doc = Jsoup.connect(link).get();
        Thread.sleep(150);
        Elements elements = doc.select("a[href]");
//        Elements elements = doc.select("a[href^=" + link + "]");
//            Elements elementsRegex = doc.select("*[href^=" + link + "]");
        for (Element el : elements) {
            String strHref = el.attr("href");
            String strToOtherPage = switch (testLink(strHref)) {
                case (1) -> mainLink.concat(strHref.substring(1));
                case (2) -> strHref;
                default -> "";
            };

            if (!strToOtherPage.isEmpty()) {
                synchronized (metLinks) {
                    if (!metLinks.contains(strToOtherPage)) {
                        metLinks.add(strToOtherPage);
                        PageLinks newPageLink = new PageLinks(strToOtherPage, mainLink, depth + 1);
                        newTasks.add(newPageLink);
                    }
                }
            }
        }

        return newTasks;
    }

    private String makeString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t".repeat(Math.max(0, depth)));
        stringBuilder.append(link);
        return stringBuilder.toString();
    }


    //будем разделять ссылки на несколько видов: 1 - внутренняя (начинается с /) и идет с глав страницы
    //                                           2 - внешняя (содержит протокол и домен что исх главная стр
    //                                           3 - внешний ресурс (напр. другой сайт) или файл
    public int testLink(String link) {
        int status = 3;

        String strForLinkOld = firstPartMainLink + "\\/\\/.*" + secondPartMainLink + ".*";
        String strForLink = mainLink + ".*";
        Pattern patternForLink = Pattern.compile(strForLink);
        Matcher m = patternForLink.matcher(link);


        if (link.startsWith("/")) {
            status = 1;
        } else if (m.matches()) {
            status = 2;
        }

        if (!link.endsWith("/")) {
            status = 3;
        }

        return status;
    }
}
