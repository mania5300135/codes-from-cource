import net.jodah.concurrentunit.Waiter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.*;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TransactionTests {
    private static Bank bank;
    private static Account acc1;
    private static Account acc2;
    private static Account accBlocked;
    private static HashMap<String, Account> accounts = new HashMap<>();
    private static long allSum = 10000 + 500 + 50000;

    @Before
    public void setup() {
        acc1.setMoney(10000);
        acc1.setBlocked(false);
        acc2.setMoney(500);
        acc2.setBlocked(false);
        accBlocked.setMoney(50000);
        accBlocked.setBlocked(true);

    }

    @BeforeClass
    public static void initial() {
        acc1 = new Account(10000, "1", false);
        acc2 = new Account(500, "2", false);
        accBlocked = new Account(50000, "3", true);
        accounts.put("1", acc1);
        accounts.put("2", acc2);
        accounts.put("3", accBlocked);
        accBlocked.setBlocked(true);
        bank = new Bank(accounts);
    }

    @Test
    public void testWithBlockedAcc() throws TimeoutException {
        Waiter waiter = new Waiter();
        try {
            new Thread(() ->
            {
                try {
                    bank.transfer("3", "1", 100);
                } catch (TransferExceptions transferExceptions) {
                    assertEquals(50000, accBlocked.getMoney());
                    transferExceptions.printStackTrace();
                }
            }).start();
            new Thread(() -> {
                try {
                    bank.transfer("1", "2", 10);
                    assertEquals(9990, acc1.getMoney());
                    assertEquals(510, acc2.getMoney());
                } catch (TransferExceptions transferExceptions) {
                    assertEquals(10000, acc1.getMoney());
                    assertEquals(500, acc2.getMoney());
                    transferExceptions.printStackTrace();
                }
            /*waiter.assertEquals("9990", Long.toString(acc1.getMoney()));
            waiter.assertEquals("510", Long.toString(acc2.getMoney()));*/
                waiter.resume();
            }).start();
            waiter.await(1, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }

    @Test
    public void transferMore5k() {
        Waiter waiter = new Waiter();
        accBlocked.setBlocked(false);
        try {
            new Thread(() ->
            {
                try {
                    bank.transfer("1", "2", 5001);
                    if (acc1.getMoney() == 4999  && acc2.getMoney() == 5501) {
                        assertTrue(true);
                    } /*else {
                        assertTrue(false);
                        || acc1.getMoney() == 5299)
                    }*/
                } catch (TransferExceptions transferExceptions) {
                    if (acc1.isBlocked() && acc2.isBlocked()) {
                        assertEquals(10000, acc1.getMoney());
                        assertEquals(500, acc2.getMoney());
                    }
                    transferExceptions.printStackTrace();
                }
            }).start();

            new Thread(() -> {
                try {
                    bank.transfer("3", "1", 300);
                    if ((acc1.getMoney() == 4999 || acc1.getMoney() == 10300) && accBlocked.getMoney() == 49700) {
                        assertTrue(true);
                    } /*else {
                        assertTrue(false);
                    }*/
                } catch (TransferExceptions transferExceptions) {
                    if (accBlocked.isBlocked() || acc1.isBlocked()) {
                        assertEquals(50000, accBlocked.getMoney());
                        if (acc1.getMoney() == 500 || acc1.getMoney() == 5501) {
                            assertTrue(true);
                        } else {
                            assertTrue(false);
                        }
                    }
                    transferExceptions.printStackTrace();
                }
                waiter.resume();
            }).start();
            waiter.await(3, TimeUnit.SECONDS);
        } catch (InterruptedException | TimeoutException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void checkLotsOfThreads() throws InterruptedException {
        ArrayList<Thread> allThreads = new ArrayList<>();

        for (int i = 0; i < 10_000; i++) {
            allThreads.add(new Thread(() -> {
                int leftSide = 1;
                int rightSide = 4;
                int randomAcc1 = (int) (( Math.random() * (rightSide - leftSide) ) + leftSide);
                int randomAcc2 = (int) (( Math.random() * (rightSide - leftSide) ) + leftSide);
                long moneyToTransfer = (long ) (( Math.random() * (rightSide * 20_000 - leftSide) ) + leftSide);
                try {
                    bank.transfer(String.valueOf(randomAcc1), String.valueOf(randomAcc2), moneyToTransfer);
                } catch (TransferExceptions e) {
                    e.printStackTrace();
                }
            }
            ));
        }

        allThreads.forEach(Thread::start);

        Thread.sleep(5000);

        long sum = 0;

        for (Account acc : accounts.values())
        {
            sum += acc.getMoney();
        };
//        System.out.println(allSum + " " + sum);
        assertEquals(allSum, sum);


    }
}

