import java.util.HashMap;
import java.util.Random;

public class Bank
{
    private HashMap<String, Account> accounts;
    private final Random random = new Random();

    public Bank(HashMap<String, Account> accounts) {
        this.accounts = accounts;
    }

    public synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)
        throws InterruptedException
    {
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами.
     * Если сумма транзакции > 50000, то после совершения транзакции,
     * она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка
     * счетов (как – на ваше усмотрение)
     */
    public void transfer(String fromAccountNum, String toAccountNum, long amount) throws TransferExceptions{
        Account accountFrom = accounts.get(fromAccountNum);
        Account accountTo = accounts.get(toAccountNum);
        int comparing = fromAccountNum.compareTo(toAccountNum);
        try {
            if (comparing == 0) {
                throw new TransferExceptions("Невозможно выполнить транзакцию: заданы одинаковые счета");
            }
            synchronized (comparing > 0 ? accountTo : accountFrom) {
                synchronized (comparing > 0 ? accountFrom : accountTo) {
                    checkingTransfer(accountFrom, accountTo, amount);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    

    private void doTransfer (Account from, Account to, long amount) {
        from.setMoney(from.getMoney() - amount);
        to.setMoney(to.getMoney() + amount);
    }

    private void checkingTransfer(Account accountFrom, Account accountTo, long amount) throws Exception {
        String fromAccountNum = accountFrom.getAccNumber();
        String toAccountNum = accountTo.getAccNumber();
        String exString = "";
        if (accountFrom.isBlocked()) {
            throw new TransferExceptions("Счет " + fromAccountNum + " заблокирован");
        }
        if (accountTo.isBlocked()) {
            throw new TransferExceptions("Счет " + toAccountNum + " заблокирован");
        }
        if (accountFrom.getMoney() < amount) {
            throw new TransferExceptions("На счету " + fromAccountNum + " недостаточно средств");
        } else {
            if (amount > 5000) {
                if (isFraud(fromAccountNum, toAccountNum, amount)) {
                    accountFrom.setBlocked(true);
                    accountTo.setBlocked(true);
                    exString = "Счета " + fromAccountNum + " и " + toAccountNum + " заблокированы";
                    throw new TransferExceptions(exString);
                } else {
                    doTransfer(accountFrom, accountTo, amount);
                }

            } else {
                doTransfer(accountFrom, accountTo, amount);
            }
        }
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    public long getBalance (String accountNum)
    {
        Account account = accounts.get(accountNum);
        synchronized (account) {
            return account.getMoney();
        }
    }
}
