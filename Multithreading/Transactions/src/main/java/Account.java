public class Account
{
    private long money;
    private String accNumber;
    private boolean blocked = false;

    public Account(long money, String accNumber, boolean blocked) {
        this.money = money;
        this.accNumber = accNumber;
        this.blocked = blocked;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
