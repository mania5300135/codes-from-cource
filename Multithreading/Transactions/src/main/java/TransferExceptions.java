public class TransferExceptions extends Exception {
    public TransferExceptions(String message) {
        super(message);
    }
}
