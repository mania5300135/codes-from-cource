import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        String srcFolder = "C:\\Users\\Мария\\skillbox\\java_basics\\11_Multithreading\\src";
        String dstFolder = "C:\\Users\\Мария\\skillbox\\java_basics\\11_Multithreading\\dst";

        File srcDir = new File(srcFolder);
        File[] files = srcDir.listFiles();

        int cores = Runtime.getRuntime().availableProcessors();
        int imagesCount = files.length/ cores;
        int startPos = 0;
        File[] newFiles;

        if (imagesCount == 0) {
            for (File file : files) {
                newFiles = new File[1];
                System.arraycopy(files, startPos, newFiles, 0, 1);
                ImgRszrThreads thread1 = new ImgRszrThreads(newFiles, dstFolder);
                startPos+=1;
                new Thread(thread1).start();
            }
        }
        else {
            for (int i = 0; i < cores; ++i) {
                if (i == cores-1) {
                    newFiles = new File[files.length - i * imagesCount];
                    System.arraycopy(files, startPos, newFiles, 0, files.length - i * imagesCount);
                }
                else {
                    newFiles = new File[imagesCount];
                    System.arraycopy(files, startPos, newFiles, 0, imagesCount);
                }
                startPos += newFiles.length;
                ImgRszrThreads thread1 = new ImgRszrThreads(newFiles, dstFolder);
                new Thread(thread1).start();
            }
        }


    }
}
