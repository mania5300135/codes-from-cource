import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

public class ImgRszrThreads  implements Runnable {
    private File[] files;
    private String dstFolder;

    public ImgRszrThreads(File[] files, String dstFolder) {
        this.files = files;
        this.dstFolder = dstFolder;
    }

    public void run() {
        try
        {
            long start = System.currentTimeMillis();

            for(File file : files)
            {
                BufferedImage image = ImageIO.read(file);
                if(image == null) {
                    continue;
                }

                int newWidth = 300;
                int newHeight = (int) Math.round(
                        image.getHeight() / (image.getWidth() / (double) newWidth)
                );
                BufferedImage newImage = Scalr.resize(image, Scalr.Method.QUALITY, Scalr.Mode.FIT_TO_WIDTH,
                        newWidth, newHeight, Scalr.OP_ANTIALIAS);

                File newFile = new File(dstFolder + "/" + file.getName());
                ImageIO.write(newImage, "jpg", newFile);
            }
            System.out.println("Duration: " + (System.currentTimeMillis() - start));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
