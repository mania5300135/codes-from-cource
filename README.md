# Codes from course

Содержит коды с различных курсов

***

## Описание
В данном проекте представлены Java-проекты, которые были созданы во время прохождения курсов. Внутри проекта несколько папок, каждая из которых отвечает за определенный раздел. 
- Multithreading (многопоточность). Содержит 4 проекта по многопоточности: [Deadlock](https://gitlab.com/mania5300135/codes-from-cource/-/tree/main/Multithreading/Deadlock), [ImageResizer](https://gitlab.com/mania5300135/codes-from-cource/-/tree/main/Multithreading/ImageResizer), [SiteMap](https://gitlab.com/mania5300135/codes-from-cource/-/tree/main/Multithreading/SiteMap) и [Transactions](https://gitlab.com/mania5300135/codes-from-cource/-/tree/main/Multithreading/Transactions). Каждый проект (кроме Deadlock) содержит свой файл readme.txt, в котором находится краткое описание

## Статус проекта
Так как обучение еще не закончено, репозиторий будет постоянно обновляться. 
